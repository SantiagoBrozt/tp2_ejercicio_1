package ejercicio_1.modelo;

public class FakeComunicarInscripcion implements ComunicarInscripcion{
	
	private String mensaje = "";
	
	@Override
	public void comunicar(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public boolean seComunico(String mensaje) {
		return this.mensaje.equals(mensaje);
	}
}
