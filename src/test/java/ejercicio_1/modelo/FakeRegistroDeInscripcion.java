package ejercicio_1.modelo;

import java.time.LocalDate;

public class FakeRegistroDeInscripcion implements RegistroDeInscripcion {
	private String registro = "";
		
	public boolean seRegistro(String consulta) {
		return this.registro.equals(consulta);
	}
	
	@Override
	public void registrarInscripcion(LocalDate fecha, String nombreConcurso, String nombreParticipante, String apellido, int dni,
			int puntos) {
		String registro = fecha + ", " + nombreParticipante + " " + apellido + ", " + nombreConcurso + "";
		this.registro = registro;
	}
}
