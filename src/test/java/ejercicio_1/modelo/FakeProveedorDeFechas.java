package ejercicio_1.modelo;

import java.time.LocalDate;
import java.util.function.Supplier;

public class FakeProveedorDeFechas implements Supplier<LocalDate> {

	@Override
	public LocalDate get() {
		return LocalDate.of(2024, 04, 06);
	}
}
