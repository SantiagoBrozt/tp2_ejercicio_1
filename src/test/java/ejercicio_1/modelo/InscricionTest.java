package ejercicio_1.modelo;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;

public class InscricionTest {
    @Test
    public void inscripcionSimple() {
        //inicialización
    	var fakeComunicacion = new FakeComunicarInscripcion();
    	var fakeRegistro = new FakeRegistroDeInscripcion();
    	var fakeProveedorDeFecha = new FakeProveedorDeFechas();
        var fechaActual = fakeProveedorDeFecha.get();
        System.out.println(fechaActual);
        var concurso = new Concurso("Concurso de ayer", fechaActual.minusDays(1), fechaActual.plusDays(1), 
        		fakeRegistro, fakeProveedorDeFecha, fakeComunicacion); 
        //proceso
		concurso.inscribirParticipante("Santiago", "Brozt", 42234317);
		//verificación
		assertTrue(fakeRegistro.seRegistro("2024-04-06, Santiago Brozt, Concurso de ayer"));
		assertTrue(fakeComunicacion.seComunico("Si se pudo burro !"));
    }
    @Test
    public void inscripcionPrimerDia(){
        //inicialización
    	var fakeComunicacion = new FakeComunicarInscripcion();
    	var fakeRegistro = new FakeRegistroDeInscripcion();
    	var fakeProveedorDeFecha = new FakeProveedorDeFechas();
        var fechaActual = fakeProveedorDeFecha.get();
        var concurso = new Concurso("Concurso de hoy", fechaActual, fechaActual.plusDays(1), 
        		fakeRegistro, fakeProveedorDeFecha, fakeComunicacion);
        var participante = new Participante("Pepe", "Ramirez", 42234318);
        //proceso
        participante.agregarPuntos(10);
		concurso.inscribirParticipante("Pepe", "Ramirez", 42234318);
        //verificacion
        assertTrue(concurso.puntosParticipante(42234318) == 10);
        assertTrue(fakeRegistro.seRegistro("2024-04-06, Pepe Ramirez, Concurso de hoy"));
		assertTrue(fakeComunicacion.seComunico("Si se pudo burro !"));

    }
    @Test
    public void FueraDeTermino(){
        //inicialización
    	var fakeComunicacion = new FakeComunicarInscripcion();
    	var fakeRegistro = new FakeRegistroDeInscripcion();
    	var fakeProveedorDeFecha = new FakeProveedorDeFechas();
        var fechaActual = fakeProveedorDeFecha.get();
        var concursoFuturo = new Concurso("Concurso futuro", fechaActual.plusDays(1), fechaActual.minusDays(2), 
        		fakeRegistro, fakeProveedorDeFecha, fakeComunicacion);
        var concursoPasado = new Concurso("Concurso pasado", fechaActual.minusDays(2), fechaActual.minusDays(1), 
        		fakeRegistro, fakeProveedorDeFecha, fakeComunicacion);
        var participante = new Participante("Luca", "Modric", 42234319);
        //proceso
        try {
			concursoFuturo.inscribirParticipante("Luca", "Modric", 42234319);
		} 
        catch (RuntimeException e) {
			e.printStackTrace();
		}
        try {
			concursoPasado.inscribirParticipante("Luca", "Modric", 42234319);
		} 
        catch (RuntimeException e) {
			e.printStackTrace();
		}
        //verificacion
        assertFalse(fakeRegistro.seRegistro("2024-04-06, Luca Modric, Concurso futuro"));
        assertFalse(fakeRegistro.seRegistro("2024-04-06, Luca Modric, Concurso pasado"));
		assertFalse(fakeComunicacion.seComunico("Si se pudo burro !"));

    }
}
