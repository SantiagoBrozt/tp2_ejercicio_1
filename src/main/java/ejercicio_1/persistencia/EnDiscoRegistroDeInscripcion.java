package ejercicio_1.persistencia;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;

import ejercicio_1.modelo.RegistroDeInscripcion;

public class EnDiscoRegistroDeInscripcion implements RegistroDeInscripcion{
	
	private String localPath; 
	
	public EnDiscoRegistroDeInscripcion(String path) {
		this.localPath = path;
	}
	@Override
	public void registrarInscripcion(LocalDate fecha, String nombreConcurso, String nombreParticipante, String apellido, int dni,
			int puntos) {
		String registro = fecha + ", " + nombreParticipante + " " + apellido + ", " 
			+ nombreConcurso + "\n";
		try {
        	Files.write(Paths.get(this.localPath), 
        			registro.getBytes(), 
        			StandardOpenOption.CREATE, StandardOpenOption.APPEND);		
        }
        catch (IOException e) {
        	throw new RuntimeException("no se ha podido persistir", e);
        }			
	}
}
	
