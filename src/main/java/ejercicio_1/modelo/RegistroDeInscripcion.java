package ejercicio_1.modelo;

import java.time.LocalDate;

public interface RegistroDeInscripcion {
	void registrarInscripcion(LocalDate fecha, String nombreConcurso, String nombreParticipante, String apellido, 
			int dni, int puntos);
}
