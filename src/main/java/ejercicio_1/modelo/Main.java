package ejercicio_1.modelo;

import java.time.LocalDate;
import java.util.ServiceLoader.Provider;
import java.util.function.Predicate;
import java.util.function.Supplier;

import ejercicio_1.comunicacion.PorEmailComunicarInscripcion;
import ejercicio_1.persistencia.EnDataBaseRegistroDeInscripcion;
import ejercicio_1.persistencia.EnDiscoRegistroDeInscripcion;

public class Main {
	public static void main(String[] args) {
		
		//registro la inscripción en disco
		var proveedorDeFecha = new Supplier<LocalDate>() {
			@Override public LocalDate get() {
				return LocalDate.now();
			}
		};
		var fechaActual = proveedorDeFecha.get();
        var concurso = new Concurso("Concurso de ayer", fechaActual.minusDays(1), fechaActual.plusDays(1), 
        		new EnDiscoRegistroDeInscripcion("/home/sancho/Documentos/Inscriptos.txt"), proveedorDeFecha, new PorEmailComunicarInscripcion()); 
        concurso.inscribirParticipante("Santiago", "Brozt", 42234317);
        //registro la inscripción en base de datos
        concurso = new Concurso("Concurso de ayer", fechaActual.minusDays(1), fechaActual.plusDays(1),  
        		new EnDataBaseRegistroDeInscripcion(), proveedorDeFecha, new PorEmailComunicarInscripcion());
        concurso.inscribirParticipante("Santiago", "Brozt", 42234317);
        //registro la inscripción en base de datos
		concurso = new Concurso("Concurso de hoy", fechaActual, fechaActual.plusDays(1), 
        		new EnDataBaseRegistroDeInscripcion(), proveedorDeFecha, new PorEmailComunicarInscripcion());
		concurso.inscribirParticipante("Santiago", "Brozt", 42234317);
	}
}
