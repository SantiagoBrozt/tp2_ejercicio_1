package ejercicio_1.modelo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader.Provider;
import java.util.function.Supplier;

import javax.management.RuntimeErrorException;

class Concurso {

    private String nombre;
    private LocalDate fechaApertura;
    private LocalDate fechaCierre;
    private List<Participante> participantes;
    private RegistroDeInscripcion registro;
    private Supplier<LocalDate> proveedorDeFecha;
    private ComunicarInscripcion comunicador;
    
    Concurso(String nombre, LocalDate apertura, LocalDate cierre, RegistroDeInscripcion registro, Supplier<LocalDate> proveedorDeFecha, ComunicarInscripcion comunicador ){
    	this.nombre = nombre;
        this.fechaApertura = apertura;
        this.fechaCierre = cierre;
        this.participantes = new ArrayList<Participante>();
        this.registro = registro;
        this.proveedorDeFecha = proveedorDeFecha;
        this.comunicador = comunicador;
    }
    
    boolean estaInscripto(int dni){
        boolean inscripto = false;
        for (Participante participante : this.participantes){
            if (participante.esTuDni(dni)){
                inscripto = true;
            }
        }
        return inscripto;
    }
    
    void inscribirParticipante(String nombreParticipante, String apellido, int dni) {
    	LocalDate fechaActual = this.proveedorDeFecha.get();
        if (!estaInscripto(dni)) {
        	if (fechaActual.isAfter(this.fechaApertura.minusDays(1)) 
        			&& (!fechaActual.isAfter(this.fechaCierre))) {
	            var nuevoParticipante = new Participante(nombreParticipante, apellido, dni);
	            if (fechaActual.isEqual(this.fechaApertura)) {
	                nuevoParticipante.agregarPuntos(10);
	            }
	            this.participantes.add(nuevoParticipante);
	            this.registro.registrarInscripcion(fechaActual, this.nombre, nombreParticipante, apellido, dni, nuevoParticipante.getPuntos());
	            this.comunicador.comunicar("Si se pudo burro !");
        	}
	        else {
	        	throw new RuntimeException("está fuera de fecha");
	        }
        }
        else {
        	throw new RuntimeException("ya está inscripto");
        }
    }

	int puntosParticipante(int dni) {
    	int puntos = 0;
        if (this.estaInscripto(dni)){
        	for (Participante participante : this.participantes){
        		if (participante.esTuDni(dni)){
        			puntos = participante.getPuntos();
        			        	
        		}
        	}
        }
        else {
        	throw new RuntimeException("no hay alguien inscripto con ese dni");
        }
        return puntos;
    }
}
